# Plural (idea-sharing iOS Application)

![Github](https://img.shields.io/github/license/mashape/apistatus.svg) ![Cocoapods](https://img.shields.io/badge/platform-iOS-blue.svg)

Plural 💡 is an iOS application that allows you to submit and validate ideas for anything. In this idea-based network, we seek to allow users to seamlessly share their ideas, improvements, and suggestions for all to see that they might one day pursue something worthwhile.



Here are some of the screens below:

| login | sign-up | picker | main |
|---|---|---|---|
| <img src="https://github.com/tmprk/Plural-iOS/blob/master/Images/login.png">  | <img src="https://github.com/tmprk/Plural-iOS/blob/master/Images/signup.png">  | <img src="https://github.com/tmprk/Plural-iOS/blob/master/Images/picker.png">  | <img src="https://github.com/tmprk/Plural-iOS/blob/master/Images/main.png">  |

| new post | swipe | expand | viewer |
|---|---|---|---|
| <img src="https://github.com/tmprk/Plural-iOS/blob/master/Images/newpost.png">  | <img src="https://github.com/tmprk/Plural-iOS/blob/master/Images/swipe.png">  | <img src="https://github.com/tmprk/Plural-iOS/blob/master/Images/expand.png">  | <img src="https://github.com/tmprk/Plural-iOS/blob/master/Images/search.png">  |

## Requirements

- Xcode 9
- iOS 9
- Swift 3.2

## Contributions

Feel free to suggest edits and improvements.

## License <a name="license"></a>

Plural-iOS is available under the MIT license. See the LICENSE file for more info.
